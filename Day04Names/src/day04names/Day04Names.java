/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day04names;

import java.util.*;
public class Day04Names {


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("How many names you want to input:");
        int total=input.nextInt();
        
        String[] names = new String[total];
        input.nextLine();
        
        for (int i = 0; i < total; i++) {
            System.out.print("Enter name #"+ (i+1) + ":" );
            names[i]= input.next();
        }
        String pnames="";
        //pnames = Arrays.toString(names);
        //pname = String.join(", ", names);
        //pnames = pnames.replace("[", "").replace("]","");
        for (int i = 0; i < total; i++) {
            pnames += (i==0 ? "":", ") + names[i];
        }
        
        
        System.out.println(pnames);
    }
    
}
