package day11progcalc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day11ProgCalc {

    static LIFOStack<Double> stack = new LIFOStack<>();
    static String regDouble = "^[-+]?[0-9]*\\.?[0-9]+$";
    static ArrayList<String> files = new ArrayList<String>();
    static Scanner input = new Scanner(System.in);
    
    public static void getFiles() {        
        File directory = new File(".");
        try{
            directory.getCanonicalPath();
            File[] tempList = directory.listFiles();
            for (int i = 0; i < tempList.length; i++) {
                if (tempList[i].isFile()) {
                    if(tempList[i].getName().endsWith(".txt"))
                    files.add(tempList[i].getName());
                }
//                if (tempList[i].isDirectory()) {
//        //              System.out.println("文件夹：" + tempList[i]);
//                }
            }
        }catch(IOException ex){
            System.out.println("Directory is null");
        }
    }
    
    public static void execFile(String filename){
        try (Scanner fileInput = new Scanner(new File(filename))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                // is it a floating point number? yes - push it
                try {
                    double numVal = Double.parseDouble(line);
                    stack.push(numVal);
                    continue;
                } catch (NumberFormatException ex) {
                    // do nothing, just continue below
                }
                // 
                switch (line) {
                    case "+": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v1 + v2;
                        stack.push(result);
                        continue;
                    }
                    case "-": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v1 - v2;
                        stack.push(result);
                        continue;
                    }
                    case "*": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v1 * v2;
                        stack.push(result);
                        continue;
                    }
                    case "/": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v2 / v1;
                        stack.push(result);
                        continue;
                    }
                    case "%": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v1 % v2;
                        stack.push(result);
                        continue;
                    }
                    case "=": {
                        double value = stack.peek();
                        System.out.printf("Top of stack: %.4f\n", value);
                        continue;
                    }
                    case "pop": {
                        stack.pop();
                        continue;
                    }
                    default:
                        // if (line.charAt(0) == '?') { }
                        if (line.matches("\\?:.*")) {
                            String message = line.split(":", 2)[1];                            
                            System.out.print(message);
                            double value = input.nextDouble();
                            stack.push(value);
                        } else {
                            System.out.println("warning: invalid line, skipping");
                        }                    
                } //end of switch
                //
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }
    
    public static void main(String[] args) {
        getFiles();        
        
        for (int i = 0; i < files.size(); i++) {
            System.out.printf("%d# %s\n",i+1,files.get(i));
        }
        System.out.print("Your choice is:");
        int i = input.nextInt();
        System.out.println("");
        execFile(files.get(i-1));
    }

}

class LIFOStack<T> {

    private ArrayList<T> storage = new ArrayList<>();

    void push(T item) {
        storage.add(0, item);
    }

    T pop() { // throw IndexOutOfBoundsException on empty
        if (storage.isEmpty()) {
            throw new IndexOutOfBoundsException("Stack empty");
        }
        return storage.remove(0);
    }

    T peek() { // returns top of the stack without taking it off
        if (storage.isEmpty()) {
            throw new IndexOutOfBoundsException("Stack empty");
        }
        return storage.get(0);
    }

    int size() {
        return storage.size();
    }
    // also implement toString that prints all items in a single line, comma-separated

    @Override
    public String toString() {
        return "LIFOStack" + storage.toString();
    }
}
