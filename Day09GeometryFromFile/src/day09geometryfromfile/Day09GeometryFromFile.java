package day09geometryfromfile;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day09GeometryFromFile {

    static ArrayList<GeoObj> geoList = new ArrayList<>();
    
    final static String fileName = "objects.txt";

    static void loadDataFromFile() {
        try ( Scanner fileInput = new Scanner(new File(fileName))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                try {                    
                    geoList.add(GeoObj.createFromLine(line));
                } catch (DataInvalidException ex) {
                    System.out.printf("%s : %s\n", line,ex.getMessage());
                } catch (NotSupportException ex) {
                    System.out.printf("%s : %s\n", line,ex.getMessage());
                } catch (NumberFormatException ex)
                {
                    System.out.printf("%s : %s\n", line,ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        
        loadDataFromFile();
        System.out.println("------------------------\n");
        for (GeoObj goe : geoList) {
            System.out.println(goe);
        }
    }

}

//checked exception
class DataInvalidException extends Exception {

    public DataInvalidException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public DataInvalidException(String msg) {
        super(msg);
    }
}
//unchecked exception
class NotSupportException extends RuntimeException {
    public NotSupportException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public NotSupportException(String msg) {

        super(msg);
    }
}

class GeoObj {

    private String color;

    public GeoObj(String color) throws DataInvalidException {
        setColor(color);
    }

    public void setColor(String color) throws DataInvalidException {
        String regEx = "[^a-zA-Z -]"; //website regex101.com
        if (color.matches(regEx)) {
            throw new DataInvalidException("Task can not contain a semicolon or | or `.");
        }
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "GeoObj{" + "color=" + getColor() + '}';
    }

    static GeoObj createFromLine(String dataLine) throws DataInvalidException,NotSupportException {
        String[] strArray = dataLine.split(";");
        if(strArray.length<2)
            throw new DataInvalidException("Invalided data line!");
        if(strArray[1].isEmpty())
            throw new DataInvalidException("Color must be defined!");
        switch (strArray[0]) {
                case "Point":
                    if(strArray.length!=2)
                        throw new DataInvalidException("Invalided Point data!");
                    return new Point(strArray[1]);
                case "Rectangle":
                    if(strArray.length!=4)
                        throw new DataInvalidException("Invalided Rectangle data!");
                    return new Rectangle(strArray[1],Double.parseDouble(strArray[2]),Double.parseDouble(strArray[3]));
                case "Square":
                    if(strArray.length!=3)
                        throw new DataInvalidException("Invalided Square data!");
                    return new Square(strArray[1],Double.parseDouble(strArray[2]),Double.parseDouble(strArray[2]),Double.parseDouble(strArray[2]));
                case "Circle":
                    if(strArray.length!=3)
                        throw new DataInvalidException("Invalided Circle data!");
                    return new Circle(strArray[1],Double.parseDouble(strArray[2]));
                case "Sphere":
                    if(strArray.length!=3)
                        throw new DataInvalidException("Invalided Sphere data!");
                    return new Sphere(strArray[1],Double.parseDouble(strArray[2]));
                case "Hexagon":
                    if(strArray.length!=3)
                        throw new DataInvalidException("Invalided Hexagon data!");
                    return new Hexagon(strArray[1],Double.parseDouble(strArray[2]));
                default:
                    throw new DataInvalidException("Not define this Geometry!");
        }        
    }
    
//    public abstract double getSurface();
//
//    public abstract double getCircumPerime();
//
//    public abstract int getVerticesCount();
//
//    public abstract int getEdgesCount();

    void Print() {
        System.out.println(""+getClass().getName());
    }
}

class Point extends GeoObj {

    public Point(String color) throws DataInvalidException {
        super(color);
    }

    @Override
    public String toString() {
        return "Point{" + "color=" + super.getColor() + '}';
    }
    
    
}

class Rectangle extends GeoObj {
    private double width;
    private double height;

    public Rectangle(String color, double width, double height) throws DataInvalidException {
        super(color);
        setWidth(width);
        setHeight(height);
    }

    @Override
    public String getColor() {
        return super.getColor(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "Rectangle{"+ "color=" + super.getColor() + ",Width="+ getWidth() +",Height="+ getHeight() +"}";
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        
        return height;
    }
}

class Square extends Rectangle {

    private double EdgeSize;

    public Square(String color, double width, double height, double EdgeSize) throws DataInvalidException {
        super(color, width, height);
        setEdgeSize(EdgeSize);
    }

    public void setEdgeSize(double EdgeSize) {
        this.EdgeSize = EdgeSize;
    }

    public double getEdgeSize() {
        return EdgeSize;
    }

    @Override
    public String toString() {
        return "square{" + "color=" + super.getColor() +  ", EdgeSize=" + getEdgeSize() + '}';
    }

}

class Circle extends GeoObj {

    private double radius;

    public Circle(String color, double radius) throws DataInvalidException {
        super(color);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle{" + "color=" + super.getColor() +  ", radius=" + getRadius() + '}';
    }

}

class Sphere extends GeoObj {
    private double radius;

    public Sphere(String color, double radius) throws DataInvalidException {
        super(color);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Sphere{"+ "color=" + super.getColor() + ", radius=" + getRadius() + '}';
    }
}

class Hexagon extends GeoObj{
    private double Edge;

    public Hexagon(String color, double Edge) throws DataInvalidException {
        super(color);
        setEdge(Edge);
    }

    public double getEdge() {
        return Edge;
    }

    public void setEdge(double Edge) {
        this.Edge = Edge;
    }

    @Override
    public String toString() {
        return "Hexagon{"+ "color=" + super.getColor() +  ", Edge=" + getEdge() + '}';
    }
}