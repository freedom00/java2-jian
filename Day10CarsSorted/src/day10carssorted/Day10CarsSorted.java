package day10carssorted;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Day10CarsSorted {

    static ArrayList<Car> parking = new ArrayList<>();

    public static void main(String[] args) {
        parking.add(new Car("Honda", 3.5, 1990));
        parking.add(new Car("Toyota", 3.5, 1990));
        parking.add(new Car("BMW", 3.0, 1990));
        parking.add(new Car("Honda", 3.5, 1999));
        parking.add(new Car("Mazda", 2.5, 2000));
        parking.add(new Car("Acura", 3.5, 1990));
        for (Car c : parking) {
            System.out.println(c + "\n");
        }

        System.out.println("--------------------Default sorting");
        Collections.sort(parking);
        for (Car c : parking) {
            System.out.println(c + "\n");
        }

        System.out.println("--------------------Sort by SortengineSize");
        Collections.sort(parking, Car.SortEngineSize);
        for (Car c : parking) {
            System.out.println(c + "\n");
        }

        System.out.println("--------------------Sort by prodYear");
        //prodYear py = new prodYear();
        Collections.sort(parking, Car.SortProdYear);
        for (Car c : parking) {
            System.out.println(c + "\n");
        }

        System.out.println("--------------------Sort by prodYearAndMakeModel");
        Collections.sort(parking, Car.SortProdYearAndMakeModel);
        for (Car c : parking) {
            System.out.println(c + "\n");
        }
    }

}

class Car implements Comparable<Car> {

    String makeModel;
    double engineSizeL;
    int prodYear;

    public Car(String makeModel, double engineSizeL, int prodYear) {
        this.makeModel = makeModel;
        this.engineSizeL = engineSizeL;
        this.prodYear = prodYear;
    }

    @Override
    public String toString() {
        return "Car{" + "makeModel=" + makeModel + ", engineSizeL=" + engineSizeL + ", prodYear=" + prodYear + '}';
    }

    @Override
    public int compareTo(Car o) {
        return (this.makeModel.compareTo(o.makeModel));
    }

    static Comparator<Car> SortProdYear = new Comparator<Car>() {
        @Override
        public int compare(Car o1, Car o2) {
            if (o1.prodYear < o2.prodYear) {
                return -1;
            }
            if (o1.prodYear > o2.prodYear) {
                return 1;
            } else {
                return 0;
            }
        }
    };

    static Comparator<Car> SortEngineSize = new Comparator<Car>() {
        @Override
        public int compare(Car o1, Car o2) {
            if (o1.engineSizeL < o2.engineSizeL) {
                return -1;
            }
            if (o1.engineSizeL > o2.engineSizeL) {
                return 1;
            } else {
                return 0;
            }
        }
    };

    static Comparator<Car> SortProdYearAndMakeModel = new Comparator<Car>() {
        @Override
        public int compare(Car o1, Car o2) {
            if (o1.prodYear < o2.prodYear) {
                return -1;
            }
            if (o1.prodYear > o2.prodYear) {
                return 1;
            }
            if (o1.prodYear == o2.prodYear) {
                return o1.makeModel.compareTo(o2.makeModel);
            } else {
                return 0;
            }
        }
    };
}

