package day09airporttravel;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Day09AirportTravel {

    static ArrayList<Airport> airportList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        loadDataFromFile();
        while (true) {
            int choice = getMenuChoice();
            switch (choice) {
                case 1:
                    listAllAirport();
                    break;
                case 2:
                    findDistance();
                    break;
                case 3:
                    nearestDistance();
                    break;
                case 4:
                    addAirport();
                    break;
                case 0:
                    saveDataToFile();
                    System.out.println("Exiting. Good bye!");
                    return;
                default:
                    System.out.println("Invalid choice, try again.");
            }
            System.out.println();
        }
    }
    
    private static void addAirport(){
        System.out.print("Enter airport code:");
        String code = input.nextLine();
        System.out.print("Enter city:");
        String city= input.nextLine();

        double latitude, longitude;
        try {
            System.out.print("Enter latitude (double):");
            latitude = input.nextDouble();
            System.out.print("Enter longitude (double):");
            longitude = input.nextDouble();
            input.nextLine();
        } catch (InputMismatchException ex) {
            input.nextLine();
            System.out.println("A worng number of latitude. ");
            return;
        }
        try{
            airportList.add(new Airport(code, city, latitude, longitude));
        }catch(ParameterInvalidException ex){
            System.out.println("Error: "+ ex.getMessage());
        }            
    }
    
    private static void nearestDistance(){
        double f1,f2,s1,s2;
        int first=-1,second=-1;
        String airport1,airport2;
        while (true){
            System.out.print("Please enter an ariport code:");
            airport1 = input.nextLine();
            for (int i=0; i<airportList.size(); i++) {
                if(airportList.get(i).getCode().equals(airport1)){
                    first = i;
                    break;
                }
            }
            if(first==-1){
                System.out.println("Invalid code, can not find it, please try again. ");
                continue;
            }
            f1 = airportList.get(first).getLongitude();
            f2 = airportList.get(first).getLatitude();
            break;
        }
        double distance=0.0;
        for (int i=0; i<airportList.size(); i++) {
            if(airportList.get(i).getCode().equals(airport1))
                continue;
            s1 = airportList.get(i).getLongitude();
            s2 = airportList.get(i).getLatitude();
            double temp = Distance(f1,f2,s1,s2);
            if(distance==0.0 || distance>temp)
            {
                distance = temp;
                second =i;
            }
        }
        System.out.printf("The nearest airport to %s is %s, distance is %.2fkm\n",airport1,airportList.get(second).getCode(),distance);
    }
    
    private static void findDistance()    {
        double f1,f2,s1,s2;
        int first=-1,second=-1;
        String airport1,airport2;
        while (true){
            System.out.print("Please choose first ariport code:");
            airport1 = input.nextLine();
            for (int i=0; i<airportList.size(); i++) {
                if(airportList.get(i).getCode().equals(airport1)){
                    first = i;
                    break;
                }
            }
            if(first==-1){
                System.out.println("Invalid choice, can not find it, please try again. ");
                continue;
            }
            f1 = airportList.get(first).getLongitude();
            f2 = airportList.get(first).getLatitude();
            break;
        }
        while (true){
            System.out.print("Please choose second ariport code:");
            airport2 = input.nextLine();
            if(airport2.equals(airport1)){
                System.out.println("Invalid choice, same airport, please try again. ");
                continue;
            } 
            for (int i=0; i<airportList.size(); i++) {
                if(airportList.get(i).getCode().equals(airport2)){
                    second = i;
                    break;
                }
            }
            if(second==-1){
                System.out.println("Invalid choice, can not find it, please try again. ");
                continue;
            }
            s1 = airportList.get(second).getLongitude();
            s2 = airportList.get(second).getLatitude();
            break;
        }
        double distance = Distance(f1,f2,s1,s2);
        System.out.printf("The distance between %s and %s distance is %.2fkm\n", airport1,airport2,distance);
    }

    private static void listAllAirport() {
        if (airportList.isEmpty()) {
            System.out.println("There are no airport.");
            return;
        }
        for (int i = 0; i < airportList.size(); i++) {
            System.out.printf("#%d: %s\n", i + 1, airportList.get(i));
        }
    }
    static void saveDataToFile() {
        try ( PrintWriter fileOutput = new PrintWriter(new File("x" + FILE_NAME))) {
            for (Airport ap : airportList) {
                fileOutput.println(ap.saveString());
            }
        } catch (IOException ex) {
            System.out.println("Error writing file: " + ex.getMessage());
        }
    }

    static int getMenuChoice() {
        int choice;
        try{
            System.out.print("1. Show all airports (codes and city names)\n"
                + "2. Find distance between two airports by codes.\n"
                + "3. Find the 1 airport nearest to an airport given and display the distance.\n"
                + "4. Add a new airport to the list.\n"
                + "0. Exit.\n"
                + "Your choice is: ");
            choice = input.nextInt();
            input.nextLine();
        }catch(InputMismatchException ex){
            choice = -1;
            input.nextLine();
        }
        return choice;
    }

public static double Distance(double long1, double lat1, double long2,  double lat2) {  
    double a, b, R;  
    R = 6371.393; // Earth radius
    lat1 = lat1 * Math.PI / 180.0;  
    lat2 = lat2 * Math.PI / 180.0;  
    a = lat1 - lat2;  
    b = (long1 - long2) * Math.PI / 180.0;  
    double d;  
    double sa2, sb2;  
    sa2 = Math.sin(a / 2.0);  
    sb2 = Math.sin(b / 2.0);  
    d = 2  
            * R  
            * Math.asin(Math.sqrt(sa2 * sa2 + Math.cos(lat1)  
                    * Math.cos(lat2) * sb2 * sb2));  
    return d;  
} 
    
    final static String FILE_NAME = "airports.txt";
    static void loadDataFromFile() {
        try ( Scanner inputFile = new Scanner(new File(FILE_NAME))) {
            while (inputFile.hasNextLine()) {
                String line = inputFile.nextLine();
                try {
                    
                    Airport ap = new Airport(line);
                    airportList.add(ap);
                } catch (ParameterInvalidException ex) {
                    // ex.printStackTrace();
                    System.out.printf("Error: %s skipping: %s\n" , ex.getMessage(),line);
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }
}

class ParameterInvalidException extends RuntimeException {

    public ParameterInvalidException(String message) {
        super(message);
    }
}

class Airport {

    private String code;
    private String city;
    private double latitude;
    private double longitude;

    public Airport(String line) {
        String[] strArray = line.split(";");
        if (strArray.length != 4) {
            throw new ParameterInvalidException("Invalided data line!");
        }
        String regEx = "[A-Z]{3}";//website regex101.com
        String code = strArray[0];
        if (!code.matches(regEx)) {
            throw new ParameterInvalidException("Invalided Airport Code!");
        }
        regEx = "[^;]";
        String city= strArray[1];
        if (city.matches(regEx)) {
            throw new ParameterInvalidException("Invalided City Name!");
        }
        double latitude, longitude;
        try {
            latitude = Double.parseDouble(strArray[2]);
            longitude = Double.parseDouble(strArray[3]);
        } catch (NumberFormatException ex) {
            throw new ParameterInvalidException("Invalided Latitude or Longitude!");
        }
        setCode(strArray[0]);
        setCity(strArray[1]);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public Airport(String code, String city, double latitude, double longitude) {
        String regEx = "[A-Z]{3}";//website regex101.com
        if (!code.matches(regEx)) {
            throw new ParameterInvalidException("Invalided Airport Code!");
        }
        regEx = "[^;]";
        if (city.matches(regEx)) {
            throw new ParameterInvalidException("Invalided City Name!");
        }
        setCode(code);
        setCity(city);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "Airport{" + "code=" + code + ", city=" + city + ", latitude=" + latitude + ", longitude=" + longitude + '}';
    }
    
    public String saveString(){
        return String.format("%s;%s;%f;%f", getCode(),getCity(),getLatitude(),getLongitude());
    }

}
