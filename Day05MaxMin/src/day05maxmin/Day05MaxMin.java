package day05maxmin;

import java.util.*;

public class Day05MaxMin {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Double> numList = new ArrayList<>();
        
        while(true){
            System.out.print("Enter a postive number:");
            double num=input.nextDouble();
            if(num==0.0)
                break;
            numList.add(num);
        }
        double sum=0.0,average=0.0,min,max;
        for(int i=0; i < numList.size(); i++)
            sum += numList.get(i);
        average = sum / numList.size();
        Collections.sort(numList);
        min = numList.get(0);
        max = numList.get(numList.size()-1);
        System.out.printf("Sum of all numbers is: %.2f%n",sum);
        System.out.printf("Average of all numbers is: %.2f%n",average);
        System.out.printf("Maximum: %.2f%n",max);
        System.out.printf("Minimum:  %.2f%n",min);
    }
    
}
