package day06enterpeople;

import java.util.*;

public class Day06EnterPeople {
    public static void main(String[] args) {
        ArrayList<Person> peopleList = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        double totalAge=0.0;

        while (true){
            System.out.print("Enter person's name (empty to finish):");
            String name = input.nextLine();
            if(name.isEmpty())
                break;
            System.out.print("Enter age:");
            int age = input.nextInt();
            input.nextLine();
            Person p = new Person(name,age);
            peopleList.add(p);
            totalAge += age;
        }
        if (peopleList.isEmpty())
            return;
        
        int youngest=0;
        int shortest=0;
        String strSearch;
        
        System.out.print("Enter search string:");
        strSearch = input.next();
        
        double Average=totalAge/peopleList.size();
        System.out.printf("Average age is: %.2f\n",Average);
        for (int i = 0; i < peopleList.size()-1; i++) {
            if(peopleList.get(i).getAge()<peopleList.get(i+1).getAge())
                youngest = i;
            else
                youngest = i+1;
            if(peopleList.get(i).getName().length()<peopleList.get(i+1).getName().length())
                shortest = i;
            else
                shortest = i+1;
        }
        System.out.printf("Youngest person is: %s %d y/o\n",peopleList.get(youngest).getName(),peopleList.get(youngest).getAge());
        System.out.printf("Person with shortest name is: %s %d y/o\n",peopleList.get(shortest).getName(),peopleList.get(shortest).getAge());
        for (int i = 0; i < peopleList.size(); i++) {
            if(peopleList.get(i).getName().contains(strSearch))
                System.out.printf("Person with matching name: %s %d y/o\n",peopleList.get(i).getName(),peopleList.get(i).getAge());
        }
    }    
}

class Person {

    private final String name;
    private final int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void print() {
        System.out.printf("%s is %d y/o\n", this.name, this.age);
    }

    String getName() {
        return this.name;
    }

    int getAge() {
        return this.age;
    }
}