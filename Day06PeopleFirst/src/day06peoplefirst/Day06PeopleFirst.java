package day06peoplefirst;

import java.io.*;
import java.security.InvalidParameterException;
import java.util.*;

public class Day06PeopleFirst {

    static ArrayList<Person> peopleList = new ArrayList<>();

    public static void main(String[] args) {
        /*
        Person p1 = new Person("Jerry", 22);
        peopleList.add(p1);
        Person p2 = new Person("Berry", 33);
        peopleList.add(p2);
        Person p3 = new Person("Marry", 44);
        peopleList.add(p3);
        Person p4 = new Person("Warry", 55);
        peopleList.add(p4);
         */

        try ( Scanner fileInput = new Scanner(new File("people.txt"))) {
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    String name = line.split(";")[0];
                    if (line.split(";").length > 2) {
                        throw new InvalidParameterException("Error: invalid data in line, skippng:"+line);
                        //continue;
                    }
                    if (name.isEmpty()) {
                        throw new InvalidParameterException("Error: invalid data in line, skippng:"+line);
                        //continue;
                    }
                    int age = Integer.parseInt(line.split(";")[1]);//Interger.valueOf();
                    if (age > 150 || age < 0) {
                        throw new InvalidParameterException("Error: invalid data in line, skippng:"+line);
                        //continue;
                    }
                    Person p = new Person(name, age);
                    peopleList.add(p);
                } catch (NumberFormatException | InvalidParameterException ex) {
                    System.out.printf("Error parsing value:%s\n",ex.getMessage());
                    //continue;
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
            return;
        }
        if (peopleList.isEmpty())
            return;
        //peopleList.forEach(Person::print);
        int yage = -1, oage = -1;
        int age;
        for (Person p : peopleList) {
            p.print();
            age = p.getAge();
            if (yage == -1) {
                yage = age;
            }
            if (oage == -1) {
                oage = age;
            }

            if (age < yage) {
                yage = age;
            }
            if (age > oage) {
                oage = age;
            }
        }
        int[] nums = new int[5];
        nums.length
        /*
        System.out.printf("Youngest person is %d and their name is(are) ", yage);
        for (int i = 0; i < peopleList.size(); i++) {
            if (yage == peopleList.get(i).getAge()) {
                System.out.printf("%s ", peopleList.get(i).getName());
            }
        }
        System.out.println("");
        System.out.printf("Oldest person is %d and their name is(are) ", oage);
        for (int i = 0; i < peopleList.size(); i++) {
            if (oage == peopleList.get(i).getAge()) {
                System.out.printf("%s ", peopleList.get(i).getName());
            }
        }
        System.out.println("");
         */
    }

}

class Person {

    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void print() {
        System.out.printf("%s is %d y/o\n", this.name, this.age);
    }

    String getName() {
        return this.name;
    }

    int getAge() {
        return this.age;
    }
}
