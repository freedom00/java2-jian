package day05readwritefirst;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Day05ReadWriteFirst {

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner input = new Scanner(System.in);

        String fileName = "C:\\Users\\6155181\\Documents\\java2\\Day05ReadWriteFirst\\src\\day05readwritefirst\\file.txt";

        try ( PrintWriter printWriter = new PrintWriter(new FileWriter(fileName))) {
            System.out.println("Please input something:");
            String text = input.nextLine();
            for (int i = 0; i < 3; i++) {
                printWriter.printf("%s%n", text);
            }
        } catch (IOException ex) {
            Logger.getLogger(Day05ReadWriteFirst.class.getName()).log(Level.SEVERE, null, ex);
        }

        try ( Scanner fr = new Scanner(new File(fileName))) {
            while (fr.hasNextLine()) {
                System.out.println(fr.nextLine());
            }
        } catch (IOException ex) {
            Logger.getLogger(Day05ReadWriteFirst.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
