package day10queues;

import java.util.ArrayList;

public class Day10Queues {
    public static void main(String[] args) {
        LIFO<Integer> lf = new LIFO<>();
        lf.push(1);
        lf.push(2);
        lf.push(3);
        lf.push(4);
        System.out.println(lf);
        System.out.println(""+lf.pop());
        System.out.println(""+lf.peek());        
        System.out.println("----------------------");
        FIFO<Integer> ff = new FIFO<>();
        ff.add(1);
        ff.add(2);
        ff.add(3);
        ff.add(4);
        System.out.println(ff);
        System.out.println(ff.remove());
        System.out.println(ff);
    }    
}

class LIFO<T> {
	private ArrayList<T> storage = new ArrayList<>();

        public void push(T v){
            storage.add(0, v);
        }
        public T pop() throws IndexOutOfBoundsException {
            return storage.remove(0);
        }
        public T peek() throws IndexOutOfBoundsException {
            if(storage.isEmpty())
                return null;
            return storage.get(0);
        }

	public int size(){
            return storage.size();
        }        
        
        @Override
        public String toString() {
            String str="";
            for (int i = 0; i < size(); i++) {
                str += (i==0?"":",")+storage.get(i);
            }
            return str;
        }
}

class FIFO<T> {
	private ArrayList<T> storage = new ArrayList<>();
	public void add(T t){
            storage.add(t);
        }
	public T remove() throws IndexOutOfBoundsException {
            if(storage.isEmpty())
                return null;
            return storage.remove(0);
        } // throws IndexOutOfBoundsException on empty
	public int size(){
            return storage.size();
        } 
        
        @Override
        public String toString() {
            String str="";
            for (int i = 0; i < size(); i++) {
                str += (i==0?"":",")+storage.get(i);
            }            
            return str;
        }
}